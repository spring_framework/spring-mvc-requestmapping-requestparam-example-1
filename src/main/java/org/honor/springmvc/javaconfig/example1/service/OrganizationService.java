package org.honor.springmvc.javaconfig.example1.service;

import java.util.List;

import org.honor.springmvc.javaconfig.example1.dao.OrganizationDao;
import org.honor.springmvc.javaconfig.example1.domain.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationService {
	
	@Autowired
	private OrganizationDao organizationDao;
	
	public List<Organization> getOrgList(){
		List<Organization> orgList = organizationDao.getAllOrganizations();
		return orgList;
	}
}
